module.exports = {
    insertingData : `INSERT INTO hiring_details (company_name,status,notes,created_date,is_verified,company_id) VALUES (?,?,?,?,?,?)`,
    selectData : `SELECT company_name,status,notes,notes,created_date FROM hiring_details ORDER BY created_date DESC`,
    selectCompanyDetails : `SELECT CORPORATE_IDENTIFICATION_NUMBER FROM companies WHERE COMPANY_NAME = ?;`,
    pubnubPublishKey :  "pub-c-16a0e67f-2d3c-4ad5-a8f5-3758fd20beb8",
    pubnubSubscribeKey :  "sub-c-45bd19d0-feec-11e9-be22-ea7c5aada356"
}