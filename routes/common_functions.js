module.exports = {
    successResponse : function(res,data){
        res.status(200).send({
            data : data
        });
    },
    errorResponse : function(res,error){
        res.status(500).send({
            error : error
        });
    }
}