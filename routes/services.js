var express = require('express');
var router = express.Router();
var requests = require('requests');
const xmlToJson = require('xml-to-json-stream');
const parser = xmlToJson({ attributeMode: false });
var PubNub = require('pubnub');

var connection = require('../config/db_con');
var common_functions = require('./common_functions');
var common_queries = require('../config/common_queries');

var pubnub = new PubNub({
  subscribeKey: common_queries.pubnubSubscribeKey,
  publishKey: common_queries.pubnubPublishKey,
  uuid: "worksight-api",
  ssl: true
});
// connection.connect(function (err) {
//   if (err) throw err;
//   console.log('connected as id ' + connection.threadId);
// });
/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});
//add record to server
router.post('/addData', function (req, res, next) {
  if (!req.body.company_name) {
    return common_functions.errorResponse(res, 'company_name is required');
  }
  if (!req.body.status) {
    return common_functions.errorResponse(res, 'status is required');
  }
  if (!req.body.notes) {
    return common_functions.errorResponse(res, 'notes is required');
  }
  let payload = req.body;

  connection.query(common_queries.selectCompanyDetails, [req.body.company_name], function (error, results, fields) {
    if (error) {
      //connection.end();
      return common_functions.errorResponse(res, error);
    } else {
      let company_id = null;
      let is_verified = false;
      if (results.length && results[0].CORPORATE_IDENTIFICATION_NUMBER) {
        console.log(results[0].CORPORATE_IDENTIFICATION_NUMBER);
        is_verified = true;
        company_id = results[0].CORPORATE_IDENTIFICATION_NUMBER;
      }
      connection.query(common_queries.insertingData, [req.body.company_name, req.body.status, req.body.notes, new Date(), is_verified, company_id], function (error, results, fields) {
        if (error) {
          //connection.end();
          return common_functions.errorResponse(res, error);
        } else {
          //connection.end();
          if (is_verified) {
            pubnub.publish({
              message: payload,
              channel: "worksight"
            },
              function (status, response) {
                if (status.error) {
                  // handle error
                  console.log("publish error" + status);
                } else {
                  console.log("published message" + payload.toString());
                }
              });
          }

          return common_functions.successResponse(res, { message: 'Data added successfully.', is_verified: is_verified });
        }
      });
    }
  });



  // https://api.linkedin.com/v1/companies/universal-name=linkedin



  // requests('https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=818fskvdpd7nmy&redirect_uri=http://localhost:3000/auth/callback&state=fooobar&scope=r_liteprofile%20r_emailaddress%20w_member_social')
  //   .on('data', function (chunk) {
  //     console.log(chunk);
  //   })
  //   .on('end', function (err) {
  //     if (err) return console.log('connection closed due to errors', err);

  //     console.log('end');
  //   });


  // requests('https://api.linkedin.com/v1/companies/universal-name=linkedin')
  //   .on('data', function (chunk) {
  //     console.log(chunk);
  //     parser.xmlToJson(chunk, (err, json) => {
  //       if (err) {
  //         //error handling
  //       }
  //       console.log(json);

  //       //json
  //       //{
  //       //  employee: {
  //       //      name: "Alex"
  //       //  }    
  //       //}
  //     });
  //   })
  //   .on('end', function (err) {
  //     if (err) return console.log('connection closed due to errors', err);

  //     console.log('end');
  //   });

});
router.get('/getData', function (req, res, next) {
  // connection.connect();
  connection.query(common_queries.selectData, function (error, results, fields) {
    if (error) {
      // connection.end();
      return common_functions.errorResponse(res, error);
    } else {
      // connection.end();
      return common_functions.successResponse(res, results);
    }
  });
});

module.exports = router;
